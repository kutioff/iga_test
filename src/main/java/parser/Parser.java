package parser;

import java.io.*;
import java.util.*;

public class Parser {
	
	private List<String> report;
	
	private String path;
	private MyDOM dom = new MyDOM();
	private String oMarker = ObjectNode.MARKER;
	private String eMarker = EventNode.MARKER;
	
	private ObjectNode currentO;
	private EventNode currentE;
	
	public Parser(String filePath) {
		path = filePath;
		report = new ArrayList<String>();
	}
	
	public MyDOM parse() {
		int lineCounter = 0;
		
		int objCounter = 0;
		int eventCounter = 0;

		boolean currentObjectError = true;
		int errObjCounter = 0;
		int errObjEventCounter = 0;
		int errEventCounter = 0;
	
		BufferedReader br = new BufferedReader(
					new InputStreamReader(
							getClass().getResourceAsStream(path)
							)
					);
		String str;
		try {
			while ((str=br.readLine()) != null) {
				lineCounter++;
				if (str.indexOf(oMarker) != -1) {
					try {
						currentO = new ObjectNode(str);
						dom.addNode(currentO);
						currentObjectError = false;
						objCounter++;
					} catch (Exception ex) {
						currentObjectError = true;
						errObjCounter++;
						log("WARNING. Object reading error. Line " + lineCounter);
					}
				} else {
					if (!currentObjectError) {
						if (str.indexOf(eMarker) != -1) {
							try {
								currentE = new EventNode(str);
								currentO.addNode(currentE);
								eventCounter++;								
							} catch (Exception ex) {
								errEventCounter++;
								log("WARNING. Event reading error. Line " + lineCounter);
							}
						}
					} else {
						errObjEventCounter++;
					}
				}		
			}
		} catch (IOException ex) {
			log("WARNING. File reading error");
		}
		try {
			br.close();
		} catch (IOException ex) {}
		log("File parsing finished.");
		log("There are " + lineCounter + " lines total.");
		log(objCounter + " objects were parsed successfully. They contain " + eventCounter + " events.");
		if (errObjCounter != 0) {
			log("WARNING. There are " + errObjCounter + " error objects , containing " + errObjEventCounter + " events.");
		}
		if (errEventCounter != 0) {
			log("WARNING. There are " + errEventCounter + " error events.");
		}
				
		return dom;
	}
	
	private void log(String message) {
		report.add(message);
	}
	
	public String[] getReport() {
		String[] result = new String[report.size()];
		report.toArray(result);
		return result;
	}
	
}
