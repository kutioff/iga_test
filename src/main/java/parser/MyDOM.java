package parser;

import java.util.*;

public class MyDOM {
	
	private List<ObjectNode> childs = new LinkedList<ObjectNode>();
	
	void addNode(ObjectNode object) {
		childs.add(object);
	}
	
	public List<ObjectNode> getChilds(){
		return childs;
	}
	
}
