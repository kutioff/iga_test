package parser;

import java.util.*;

public class ObjectNode {	
	public static final String MARKER = "businessobject";
	public String name;
	private List<EventNode> childs = new LinkedList<EventNode>();
		
	ObjectNode(String text) throws Exception {
		name = text.substring(text.indexOf(MARKER) + MARKER.length(), text.length()-1).trim();
		if (name.equals(""))
			throw new Exception();
	}
	
	void addNode(EventNode event) {
		childs.add(event);
	}
	
	public List<EventNode> getChilds(){
		return childs;
	}
	
	@Override	
	public String toString() {
		return name;
	}
}
