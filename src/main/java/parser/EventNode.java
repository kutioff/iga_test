package parser;

import java.text.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EventNode {
	public static final String MARKER = "history";
	
	public String type;
	public String user;
	public Date time;
	public Map<String, String> map = new TreeMap<String, String>();
	
	public EventNode(String text) throws Exception {		
		text = text.substring( text.indexOf(MARKER) + MARKER.length() + 3);
		String[] arr;
		arr = text.split(" - user: ");
		type = arr[0];
		user = arr[1].substring(0, arr[1].indexOf("  time: "));		
		String rest = arr[1].substring(user.length());
		
		Pattern p = Pattern.compile("  [A-Za-z]{1,}: ");
	    Matcher m = p.matcher(rest);
	    int start;
	    int end = 0;
	    int lastEnd = -1;
	    String key="";
	    String val;
	    while (m.find()) {
	    	start = m.start();
	    	end = m.end();
	    	if (lastEnd != -1) {
	    		val = rest.substring(lastEnd, start);
	    		map.put(key, val);
	    	}
	    	key = rest.substring(start+2, end-2);
	    	lastEnd = end;
	    }
	    val = rest.substring(end, rest.length());
	    map.put(key, val);
	    DateFormat format = new SimpleDateFormat("M/d/yyyy h:mm:ss a", Locale.UK);
	    time = format.parse(map.get("time"));
	    
	    if (user.equals("") || type.equals("")) {
	    	throw new Exception();
	    }
	}
	
	@Override	
	public String toString() {	
		return String.format("Event: [type: \"%s\", user:\"%s\"] ", type, user, time)
				+ " _ " + time + " _ "
				+ map.toString();
	}

}
