package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import datamodel.*;


public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private DataModel dataModel;
	private Controller controller;
	
	@Override
	public void init() {
		dataModel = new DataModel();
		dataModel.build();
		controller = new Controller(dataModel);
		String[] logs = dataModel.getLogs();
		System.out.println("������������� ��������");
		for (String s:logs) {
			System.out.println(s);
		}
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub       
        request.getRequestDispatcher("/home.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		StringBuilder buffer = new StringBuilder(); 
	    BufferedReader reader = request.getReader(); 
	    String line; 
	    while ((line = reader.readLine()) != null) { 
	    	buffer.append(line); 
	    } 
	    String body = buffer.toString();
	    
	    response.getWriter().write(
	    	controller.getResponse(body)
	    );		
	}
	

}
