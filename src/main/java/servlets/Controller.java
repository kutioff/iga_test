package servlets;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.json.simple.*;
import datamodel.*;

public class Controller {
	
	private DataModel dataModel;
	
	public Controller(DataModel model){
		dataModel = model;
	}
	
	String getResponse(String requestBody) {
		JSONObject obj = (JSONObject) JSONValue.parse(requestBody);
		switch (Integer.valueOf( (String) obj.get("type"))) {
			case 1: // Should send users and types to browser
				return JSONValue.toJSONString(getUsersAndTypes());
			case 2: // Should send events to browser
				return JSONValue.toJSONString(getEvents(obj));
			case 3: // Should send servlet logs to browser
				JSONArray arr = new JSONArray();
				arr.addAll(Arrays.asList(dataModel.getLogs()));
				return JSONValue.toJSONString(arr);
			default:
				return "{}";
		}
	}
	
	private JSONObject getUsersAndTypes() {
		JSONObject obj = new JSONObject();
		JSONArray jsArr;
		jsArr = new JSONArray();
		jsArr.addAll(Arrays.asList(dataModel.getUsers()));
		obj.put("users", jsArr);
		jsArr = new JSONArray();
		jsArr.addAll(Arrays.asList(dataModel.getTypes()));
		obj.put("types", jsArr);
		return obj;
	}
	
	public JSONArray getEvents(JSONObject obj) {
		JSONArray arr = new JSONArray();
		HashSet<String> users = new HashSet<String>();
		JSONArray jsUsers = (JSONArray) obj.get("users");
		Iterator i1 = jsUsers.iterator();
		while (i1.hasNext()) {
			users.add((String)i1.next());
		}
		HashSet<String> types = new HashSet<String>();
		JSONArray jsTypes = (JSONArray) obj.get("types");
		Iterator i2 = jsTypes.iterator();
		while (i2.hasNext()) {
			types.add((String)i2.next());
		}
		List<Event> events = dataModel.getEvents(
				(Long)obj.get("startTime"),
				(Long)obj.get("endTime"),
				users, types);
		Event temp;
		JSONObject jsTemp;
		Iterator<Event> it = events.iterator();
		while (it.hasNext()) {
			temp = it.next();
			jsTemp= new JSONObject();
			jsTemp.put("type", temp.getType());
			jsTemp.put("user", temp.getUser());
			jsTemp.put("time", temp.getTime().getTimeInMillis());
			arr.add(jsTemp);
		}
		
		return arr;
	}
	
	

}
