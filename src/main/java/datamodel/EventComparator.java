package datamodel;

import java.util.*;

public class EventComparator implements Comparator<Event> {
	
	public static final int INTERVAL = 5000;

	public int compare(Event e1, Event e2) {
		// TODO Auto-generated method stub
		
//		System.out.println(" Call Comparator");
		
		GregorianCalendar t1 = e1.getTime();
		GregorianCalendar t2 = e2.getTime();
		long timeDiff = t1.getTimeInMillis() - t2.getTimeInMillis();
		if (Math.abs(timeDiff) <= INTERVAL) {
			int compareObj = e1.getObject().compareTo(e2.getObject());
			if (compareObj == 0) {
				int compareUser = e1.getUser().compareTo(e2.getUser());
				if (compareUser == 0) {
					return e1.getType().compareTo(e2.getType());				
				} else {
					return compareUser;
				}			
			} else {
				return compareObj;
			}		
		} else {		
			return t1.compareTo(t2);
		}
	}

}
