package datamodel;

import java.util.*;
import parser.*;

public class DataModel {
	
	private final String path = "/history.txt";
	
	private String[] parserLogs;
	private List<String> dataModelLogs;
	
	private SortedSet<Event> events;
	private Set<String> types;
	private Set<String> users;
	
	
	public DataModel() {
		dataModelLogs = new LinkedList<String>();
		events = new TreeSet<Event>(new EventComparator());
		types = new HashSet<String>();
		users = new HashSet<String>();
	}
	
	public void build() {
		Parser parser = new Parser(path);
		MyDOM dom = parser.parse();		
		parserLogs = parser.getReport();
		
		List<ObjectNode> oNodes = dom.getChilds();
		List<EventNode> eNodes;
		Event event;
		GregorianCalendar calendar = new GregorianCalendar();
		for (ObjectNode oNode: oNodes) {
			eNodes = oNode.getChilds();
			for (EventNode eNode: eNodes) {
				calendar.setTime(eNode.time);
				event = new Event(oNode.name, eNode.type, eNode.user, calendar);
				if (events.add(event)) {
					types.add(event.getType());
					users.add(event.getUser());
				}
			}
		}
		
		log("Data model building finished.");
		log("Total event count: " + events.size());
		log("There are following types of events: " + types.toString());
		log("The events were initiated by following users: " + users.toString());
		
	}
	
	private void log(String message) {
		dataModelLogs.add(message);
	}
	
	public String[] getLogs() {
		List<String> logs = new LinkedList<String>();
		logs.addAll(Arrays.asList(parserLogs));
		logs.addAll(dataModelLogs);
		String[] result = new String[logs.size()];
		logs.toArray(result);
		return result;
	}
	
	public String[] getTypes() {
		String[] arr = new String[types.size()];
		types.toArray(arr);
		return arr;
	}
	public String[] getUsers() {
		String[] arr = new String[users.size()];
		users.toArray(arr);
		return arr;
	}
	
	public List<Event> getEvents(long time1, long time2, HashSet<String> users, HashSet<String> types){
		List<Event> list = new LinkedList<Event>();
		GregorianCalendar d1 = new GregorianCalendar();
		GregorianCalendar d2 = new GregorianCalendar();		
		d1.setTimeInMillis(time1);
		d2.setTimeInMillis(time2);
		d1.set(Calendar.HOUR_OF_DAY, 0);
		d1.set(Calendar.MINUTE, 0);
		d1.set(Calendar.SECOND, 0);
		d2.set(Calendar.HOUR_OF_DAY, 23);
		d2.set(Calendar.MINUTE, 59);
		d2.set(Calendar.SECOND, 59);
		Event e1 = new Event("", "", "", d1);
		Event e2 = new Event("", "", "", d2);		
		SortedSet<Event> set = events.subSet(e1, e2);
		Event temp;
		Iterator<Event> it = set.iterator();
		while (it.hasNext()) {
			temp = it.next();
			if (users.contains((String)temp.getUser()) && types.contains((String)temp.getType()))
				list.add(temp);
		}
		return list;		
	}

	

}
