package datamodel;

import java.util.*;

public class Event /* implements Comparable<Event> */ {
	
	private String object; // ��� �������, �������� ����������� �������
	private String type;
	private String user;
	private GregorianCalendar time;
	
	public Event(String object, String fullType, String user, GregorianCalendar time) {
		this.object = object;
		this.type = fullType.split(" ")[0];
		this.user = user;
		this.time = (GregorianCalendar) time.clone();
	}
	
	public String getObject() {
		return object;
	}
	public String getType() {
		return type;
	}
	public String getUser() {
		return user;
	}
	public GregorianCalendar getTime() {
		return (GregorianCalendar) time.clone();
	}


}
