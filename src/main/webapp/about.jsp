<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="java.util.*,java.text.*" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>IGA Test</title>
    <base target="_top">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

    <script type="text/javascript" src="resources/js/my_about.js"></script>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="resources/css/my.css">
  </head>
  <body>
    
    <div class="about container">
      <div class="panel">
        <div><p>
        	<a class="back_link" href="<%= request.getContextPath()%>/home">Назад на главную страницу</a>
        </p></div>

        <h2 style="clear:both">О приложении</h2>
		<p>Данное веб-приложение «IGA Test» предназначено для демонстрации возможности создания веб-приложений
		 с использованием технологий:</p>
		<ul>
			<li>Apache Tomcat;</li>
			<li>Java Sevlet, JSP;</li>
			<li>HTML, CSS, Javascript.</li>
		</ul>
        <p>Приложение предоставляет пользователю доступ к данным о событиях жизненного цикла документов PDM-системы. 
        Каждое событие характеризуется:</p>
        <ul>
			<li>пользователем, инициировавшим событие (user);</li>
			<li>типом события (type);</li>
			<li>временем (time).</li>
		</ul>
        <p>Данные отображаются в наглядном графическом виде с возможностью фильтрации по временному диапазону, 
        пользователям и типам событий. Графики в разделе «Типы событий» показывают, события каких типов происходили на выбранном временном диапазоне, 
        а графики в разделе «Пользователи» - какие пользователи инициировали события. 
        Для каждого раздела приведен как временной график с выбором интервала разбиения (день, неделя или месяц), так и 
        круговая диаграмма, обобщающая данные.
        </p>
        <h3>Техническая реализация веб-приложения</h3>
        <p>Серверная часть приложения написана с помощью Java Sevlet и JSP. Данные хранятся в war-архиве приложения в виде ресурсов 
        (файл history.txt). Для запуска приложения следует разместить war-файл в рабочей директории локального веб-сервера Apache Tomcat. 
        Приложение будет доступно по ссылке <i>http://localhost:8080/web_test_2/home</i>. При первом обращении браузера к веб-приложению 
        инициализируется java-сервлет MainServlet. Инициализация сервлета включает в себя:</p>
        <ul>
			<li>парсинг файла исходных данных history.txt;</li>
			<li>формирование в памяти объектной модели данных на основе данных парсинга.</li>
		</ul>
		<p>Ниже приведен отчет о работе парсера.</p>
		<div class="parser_report"></div>
		
      </div>
    </div>
  </body>
</html>


