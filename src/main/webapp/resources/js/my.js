/**
 * 
 */


var dateSelector;
var usersSelector;
var eventsSelector;
var loader;
var tooltip;
var usersViewPanel;
var eventsViewPanel;
var dataModel;


var USERS = [];
var EVENTS = [];
var REFRESH_TOOLTIP = 'Подсказка: нажмите «Загрузить» для обновления данных';

$(document).ready(function(){
  dateSelector = new DateSelector(); 
    
  loader = new Loader();
  tooltip = new ToolTip();
  
  usersViewPanel = new ViewPanel($('.users.panel'), "user");
  usersViewPanel.setTitle('Пользователи');
  
  eventsViewPanel = new ViewPanel($('.events.panel'), "type");
  eventsViewPanel.setTitle('Типы событий');
  
  dataModel = new DataModel();
  
  tooltip.set('Подсказка: задайте фильтры по датам, пользователям и типам событий и нажмите «Загрузить»');
  
  // Get users and events
  var request = {
		  type: "1"
  };
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
  	var data;
      if (xhr.readyState == 4) {
          data = xhr.responseText;
          console.log("Got users and events");
          data = JSON.parse(data);
          USERS = data.users;
          EVENTS = data.types;
          usersSelector = new UsersSelector(USERS);
          eventsSelector = new EventsSelector(EVENTS);
      }    
  }
  xhr.open('POST', 'home', true);
  console.log("Server request for users and event types");
  xhr.send(JSON.stringify(request));
});


function DateSelector(){
  var that = this;
  $('.select_panel.date input').eq(0).val('08/16/2012 - 11/16/2012');
  $('input[name="daterange"]').daterangepicker({
    opens: 'right'
  }, resetDate);
  
  this.start = new Date('2012-08-16');
  this.end = new Date('2012-11-16');
  
  function resetDate(start, end, label){
    dateSelector.start = new Date(start.format('YYYY-MM-DD'));
    dateSelector.end = new Date(end.format('YYYY-MM-DD'));
    tooltip.set(REFRESH_TOOLTIP);
  }  
  
  this.getStart = function(){
    return that.start;
  }
  this.getEnd = function(){
    return that.end;
  }
  
}


function UsersSelector(list){
  var div = $('.select_panel.users').eq(0);  
  var listSelector = new ListSelector(div, list);
  this.getSet = function(){
    if (listSelector.set.length == 0)
      return list;
    else
      return listSelector.set;
  }
}

function EventsSelector(list){
  var div = $('.select_panel.event_type').eq(0);  
  var listSelector = new ListSelector(div, list); 
  this.getSet = function(){
    if (listSelector.set.length == 0)
      return list;
    else
      return listSelector.set;
  }  
}


function ListSelector(div, list){
  var that = this;  
  var listDiv = div.find('.dropdown-content');
  var cardsDiv = div.find('.filter_cards');
  for (var i in list){
    listDiv.append('<a>'+list[i]+'</a>');
  }  
  var a = div.find('a');
  a.click(function(){
    that.add($(this));
  });
  
  this.set = [];
  
    
  this.add = function(a){
    var val = a.html();
    if (that.set.indexOf(val)==-1){
      that.set.push(val);
      that.rebuild();
    }
  }
  this.remove = function(i){
    var val = i.attr('name');
    var index = that.set.indexOf(val);
    if (index!=-1){
      that.set.splice(index, 1);
      that.rebuild();
    }
  }
  
  this.rebuild = function(){
    tooltip.set(REFRESH_TOOLTIP);
    cardsDiv.html('');
    a.removeClass('selected');
    for (var i in that.set){
      listDiv.find('a:contains("'+that.set[i]+'")').addClass('selected');
      cardsDiv.append(
        '<span class="filter_card">'+that.set[i]+'<i class="material-icons in_text" name="'+that.set[i]+'">close</i></span>'
      );
    }
    cardsDiv.find('i').click(function(){
      that.remove($(this));
    });   
  }
  
}


function Loader(){
  var that = this;
  $('#load_btn').click(function(){
    loader.load();
  });
  
  var isOccupied = false;
    
  this.load = function(eventsData){
    if (isOccupied)
      return;
    tooltip.clear();
    isOccupied = true;
    var start  = dateSelector.getStart();
    var end  = dateSelector.getEnd();   
    $('#load_btn').addClass('loading');
    
    // Get events list
    var request = {
  		type: "2",
  		types: eventsSelector.getSet(),
  		users: usersSelector.getSet(),
  		startTime: start.getTime(),
  		endTime: end.getTime()    	
    };
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
    	var data;
        if (xhr.readyState == 4) {
            data = xhr.responseText;
            data = JSON.parse(data);
            console.log("Got events data from server");
            that.loadEnd(data);
        }
    }
    xhr.open('POST', 'home', true);
    console.log("Server request for events data");
    xhr.send(JSON.stringify(request));    
    
    dataModel.start = start;
    dataModel.end = end;
  }

	  
  
  
  this.loadEnd = function(data){
    dataModel.update(data);
    isOccupied = false;
    $('#load_btn').removeClass('loading');
    usersViewPanel.display(true);
    eventsViewPanel.display(true);
    displayTogglerGrid(true);
    usersViewPanel.redraw();
    eventsViewPanel.redraw();
  }
  
}

function ToolTip(){
  var elem = $('.tooltip');
  this.set = function(text){
    elem.html(text);
  }
  this.clear = function(){
    elem.html('');
  }
}

function toggleGrid(toggler){
  var cond = toggler.hasClass('spin');
  if (cond) {
    toggler.removeClass('spin');
    $('.container.users, .container.events').addClass('full_width');
  } else {
    toggler.addClass('spin');
    $('.container.users, .container.events').removeClass('full_width');
  }
  try {
    usersViewPanel.redraw(true);
    eventsViewPanel.redraw(true);
  } catch (ex){}
}
function displayTogglerGrid(display){
	if (display)
		$('.grid_toggler').css('display','block');
	else
		$('.grid_toggler').css('display','none');
}

function descResult(text){
	$('.result_desc p').html(text);
}


function ViewPanel(elem, keyProp) {
  var that = this;
  var lineChartId = 'line_'+elem.attr('id');
  var pieChartId = 'pie_'+elem.attr('id');
  elem.html(''
    + '<h3 class="title"></h3>'
    + '<div class="select_panel period"><div class="period_switch btn-group"><button class="day">День</button><button class="week">Неделя</button><button class="month">Месяц</button></div></div>'
    + '<div id="'+lineChartId+'" style="min-width: 310px; height: 500px; margin: 0px  auto 100px 0px"></div>'
    + '<div id="'+pieChartId+'" style="min-width: 310px; height: 500px; margin: 0 auto"></div>'
  );
  
  this.display = function(display){
    var parent = elem.parent();
    if (display)
      parent.css('display','block');
    else
      parent.css('display','none');
  }
  
  this.setTitle = function(title){
    elem.find('.title').html(title);
  }
  
  this.lineChart = new LineChart($('#' + lineChartId), this);  
  this.pieChart = new PieChart($('#' + pieChartId), this);
        
  var cache;
  this.redraw = function(useCache){
    var start = dataModel.start;
    var end = dataModel.end;
    var data;
    if (useCache * cache)
      data = cahce;
    else {
      data = dataModel.getData(start, end, that.lineChart.getPeriod(), keyProp);
      cache = data;
    }
  
    that.lineChart.drawChart(data.periods, data.data);
    that.pieChart.drawChart(data.data);
  }
  
}



function PeriodSwitcher(div, viewPanel){
  var that = this; 
  var dayBtn = div.find('.day');
  var weekBtn = div.find('.week');
  var monthBtn = div.find('.month');
       
  dayBtn.click(function(){that.setPeriod($(this), "day")});
  weekBtn.click(function(){that.setPeriod($(this), "week")});
  monthBtn.click(function(){that.setPeriod($(this), "month")});
  
  this.period = "month";
  monthBtn.addClass('active');
  
  this.setPeriod = function(btn, period){
    div.find('button').removeClass('active');
    btn.addClass('active');
    that.period = period;
    viewPanel.redraw();
  }
}

function LineChart(elem, parent){

  var switcher = new PeriodSwitcher(elem.parent().find('.period_switch'), parent);
  
  this.getPeriod = function(){
    return switcher.period;
  }
  
  this.drawChart = function(periods, values){
  
    var series = [];
    var temp;
    for (var propVal in values){
      temp = {
        name: propVal,
        data: []
      };
      for (var j in values[propVal]){
        temp.data.push(values[propVal][j]);
      }
      series.push(temp);      
    }
    
    Highcharts.chart( elem.attr('id'), {
      chart: {type: 'line'},
      title: {text: 'Активность во времени'},
      subtitle: {text: ''},
      xAxis: {categories: periods},
      yAxis: {title:{ text: 'Количество событий'}},
      plotOptions: {line: {
        dataLabels: {enabled: true},
        enableMouseTracking: false
      }},  
      series: series
    });   
    
  }
  
}



function PieChart(elem, parent){
  
  
  this.drawChart = function(values){
    
    var data = [];
    var temp;
    for (var propVal in values){
      temp = {
        name: propVal,
        y: 0
      };
      for (var j in values[propVal]){
        temp.y += values[propVal][j];
      }
      data.push(temp);      
    }
    
    Highcharts.chart( elem.attr('id'), {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: 'Общая активность'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            style: {
              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
            }
          }
        }
      },
      series: [{
        name: 'Доля',
        colorByPoint: true,
        data: data
      }]
    });   
    
  }
  
}




var MILLIS_IN_DAY = 86400000;
var MILLIS_IN_WEEK = 604800000;
		
function DataModel(){
  var that = this;
  
  this.start;
  this.end;
  
  this.list;
  this.update = function(data){
	var date;
    descResult(''
    	+'По заданным критериям найдено '+data.length+' событий'
    	+'<br>Интервал: с '+getTitle(this.start,"day")+' по '+getTitle(this.end,"day")
    );
	for (var i in data){
		date = new Date();
		date.setTime(data[i].time);
		data[i].time = date;
	}
    this.list = data;
  }
  
  /*
  *  Возвращает объект данных со структурой
  *   {
  *     peiods:[period1, period2, ...],
  *     data:{
  *       keyProp1: [data1_1 , data1_2 , ...],
  *       keyProp2: [data2_1 , data2_2 , ...],
  *       ...
  *     },
  *   }
  *
  *  @param date1, date2 - границы запрашиваемого отрезка
  *  @param step - one of "day", "week", "month"
  *  @param keyProp - основное свойство ("user" или "type")
  */
  this.getData = function(date1, date2, step, keyProp){
      
    if (keyProp!="user" && keyProp!="type")
      throw "Ошибка преобразования данных. Свойство keyProp задано неверно";
    if (step!="day" && step!="week" && step!="month")
      throw "Ошибка преобразования данных. Свойство step задано неверно";
    
    var resPeriods = [];
    var resData = {};
    var result = {
      periods: resPeriods,
      data: resData
    };  
    
    // Формирование массива периодов
    var tempDate = date1;
    var title;
    resPeriods.push(getTitle(tempDate, step));
    var counter = 0;
    while (true){
      tempDate = inkr(tempDate, step);
      title = getTitle(tempDate, step);
      if (resPeriods[resPeriods.length-1] != title)
        resPeriods.push(title);
      if (tempDate > date2)
        break;
      counter++;
      if (counter > 10000){       //Защита от бесонечного цикла
        console.log("Число итераций превысило 1000");
        break;
      }
    }
    if (resPeriods[resPeriods.length-1] != getTitle(date2))
      resPeriods.splice(resPeriods.length-1, 1);
    
    // Формирование объекта data
    var source = that.list;
    var keyPropVal;
    var resultIndex;
    var totalCount=0;
    for (var i in source){
      keyPropVal = source[i][keyProp];
      
      // Формирование пустого массива data.keyProp
      if (!resData[keyPropVal]){
        resData[keyPropVal] = [];
        for (var j in resPeriods)
          resData[keyPropVal].push(0);
      }
      
      // Заполнение массива data.keyProp
      resultIndex = resPeriods.indexOf( getTitle(source[i].time, step) );
      if (resultIndex != -1){
        resData [keyPropVal] [resultIndex] += 1;
        totalCount++;
      }
      
    }
        
    return result;
    
    
    
  }  

  function getTitle(date, step){
      switch(step){
        case "day":
          return echoDate(date) + '.' + echoMonth(date) + '.' + (date.getFullYear()%1000);
        case "week":
          var firstWeekDate = getFirstWeekDate(date); // Monday
          return echoDate(firstWeekDate) + '.' + echoMonth(date) + '.' + (date.getFullYear()%1000) + ' ...';
        case "month":
          return echoMonth(date) + '.' + (date.getFullYear());
        default:
          return 'notitile';
      }
    }
    function echoDate(date){
      return twoDigits(date.getDate());
    }
    function echoMonth(date){
      return twoDigits(date.getMonth()+1);
    }
    function twoDigits(str){
      return ('00'+str).slice(-2);
    }
    
    function inkr(date, step){
      var k;
      switch(step){
        case "day":
          k = 1;
          break;
        case "week":
          k = 7;
          break;
        case "month":
          k = 28;
          break;
        default:
          return null;
      }
      var res = new Date();
      res.setTime(date.getTime() + k * MILLIS_IN_DAY);
      return res;
    }
  
  function equalYear(date1, date2){
    return date1.getFullYear() == date2.getFullYear();
  }
  function equalMonth(date1, date2){
    return equalYear(date1, date2) && (date1.getMonth() == date2.getMonth());
  }
  function equalDate(date1, date2){
    return equalMonth(date1, date2) && (date1.getDate() == date2.getDate());
  }
  function equalWeek(date1, date2){
    var diff = date2.getTime() - date1.getTime();
    var maxDiff = MILLIS_IN_WEEK;
    if (Math.abs(diff) >= maxDiff){
      return false;
    } else {
      return diff * ( getWeekDay(date2) - getWeekDay(date1) )  >  0;
    }
  }
  // returns number from 0 to 6
  // 0 means Monday
  // 6 means Sunday
  function getWeekDay(date){
    var day = date.getDay();
    day--;
    if (day == -1)
      day = 6;
    return day;
  }
  function getFirstWeekDate(date){
    var result = new Date();
    result.setTime(date.getTime() - getWeekDay(date) * MILLIS_IN_DAY);
    return result;
  }
  
}



function jsonReviver(key,value){
  if (typeof value == 'string'){
    if (value.length==24){
      if (value.match(/\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d.\d\d\dZ/)){
        try {
          value = new Date(value);
        } catch (e) {}
      }
    }
  }
  return value;
}











