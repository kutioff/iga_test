<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="java.util.*,java.text.*" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>IGA Test</title>
    <base target="_top">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    
       
    <!-- CHARTS -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    
    <!-- DATE RICKER -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
 
    <script type="text/javascript" src="resources/js/my.js"></script>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="resources/css/my.css">
  </head>
  <body>
    
    <div class="filter container">
      <div class="filter panel">
        
        <div class="info_button">
          <a href="/web_test_2/about.jsp"><i class="material-icons grey_button" title="О приложении">apps</i></a>
        </div>
        
        <h2>Статистика событий</h2>
        <div class="select_panel date">
          <input type="text" name="daterange" value="" />          
        </div>
        <div class="select_panel users">
          <div class="dropdown">
            <button class="dropbtn">Пользователи<i class="material-icons in_text">filter_list</i></button>
            <div class="dropdown-content">
            </div>
          </div>
          <div class="filter_cards"></div>
        </div>
        <div class="select_panel event_type">
          <div class="dropdown">
            <button class="dropbtn">Типы событий<i class="material-icons in_text">filter_list</i></button>
            <div class="dropdown-content">
            </div>
          </div>
          <div class="filter_cards"></div>
        </div>
        <div style="clear:both; padding-top:15px">
          <button class="dropbtn" id="load_btn"><span class="text">Загрузить</span></button>
          <span class="tooltip"></span>
        </div>
      </div>
    </div>
    <div class="result_desc">
    	<p></p>
    </div>
    <div class="grid_toggler">
      <i class="material-icons spin grey_button" title="Нажмите для переключения компоновки страницы" onclick="toggleGrid($(this))">view_agenda</i>
    </div>
    <div style="clear:both"></div>
    <div class="users container">
      <div class="users panel" id="user_charts_container"></div>
    </div>        
    <div class="events container">
      <div class="events panel" id="event_charts_container"></div>
    </div>   
    
    <!--
    <div class="list panel">List</div>
    -->
    
  </body>
</html>

